# Puzzle Starter Chakras
Course asset for the Udacity [VR Developer Nanodegree](http://udacity.com/vr) program.

- Course: VR Design
- Lesson: Work presentation

## Introdução
O jogo é uma espécie de "Siga a sequência". O jogo foi ressignificado em uma pequena alusão aos 7 chakras principais do corpo humano, onde o jogador se encontra em sua masmorra e através do foco, paciência, persistência e concentração aos poucos vai dissolvendo, iluminnando e transformando esse espaço virtual/mental.

## Outcomes
O jogo chegou na sua versão de apresentação, mesmo sabendo que ainda poderia ir além.
O uso das fases de iteração foram muito precioso bem como ter usuários para testar.
Veja o vídeo final abaixo.

Veja o vídeo

[![Alt text](https://img.youtube.com/vi/Zj_4I9brcx4/0.jpg)](https://www.youtube.com/watch?v=Zj_4I9brcx4)

## História do processo
O processo se inicia no curso "Desenvolvedor de realidade virtual" na sessão "Design de RV" o qual foi proposto diversos assuntos até a montagem desse game.

**O primeiro passo** foi pensar em rascunho da cena no papel.
![Alt text](docs/esboço_inicial.jpg?raw=true "Esboço")

### Primeira iteração
**A montagem da cena** foi implementada no unity3d de uma forma aproximada com o rascunho. A intenção era de observar a escala dos objetos em cena.

Antes             |  Depois
:-------------------------:|:-------------------------:
![Alt text](docs/teste_escala.png?raw=true "Escala dos objetos em cena")  |  ![Alt text](docs/teste_escala1.png?raw=true "Escala dos objetos em cena")
Após esse teste, foi percebido por mim que a escala da masmorra estava um pouco pequena, então aumentei a escala de 1 para 1,5.

**O posicionamento das Orsb (esferas)** foi implementado de forma quase circular, para não obstruir a porta de saída, com 7 Orbs na cena.
![Alt text](docs/adicao_das_orbs.png?raw=true "Adição das Orbs")
Nesse ponto também foi pensado a parte ergonômica do jogo, pois eu queria buscar comforto para o jogador.

**Para a ambientação** nesse momento foi implementada as luzes do jogo para dar um ar mais misterioso e sombrio.
![Alt text](docs/iluminacao.png?raw=true "Iluminação")
A cor da luz principal foi alterada bem como a sua intensidade, foram adicionadas "Tochas" e "Point light" nas mesmas e foi adicionado uma "Spot light" para iluminar as Orbs.

**O teste com usuários na primeira iteração**

**Pergunta 1**: Você se sente alto, baixo ou apertado no ambiente

**Pertunga 2**: Você consegue observar as esferas confortavelmente? O movimento da sua cabeça incomoda ao tentar observar uma Orb específica?

**Pergunta 3**: O que você acha do clima do ambiente?

- Jess disse que se sentiu um pouco pequena dentro da sala, que as Orbs estavam confortaveis de observar e que parecia estar em uma masmorra medieval. Foi questionado a posição da pessoa (que era quase deitada) e após novo teste na posição sentada já não se sentiu pequena no espaço.
- Rena disse que estava se sentindo um pouco alto, que o ambiente estava legal e as orbs estavam bem colocadas. Foi questionado o porque de se sentir alto e o usuário disse que o tamanho do barril o fez pensar que estava alto e após o novo teste no ambiente Rena percebeu que tinha como referência de um barril algo grande, tipo o da série "Chaves", e quando observou algo menor pensou estar mais alto e acreditou que a escala dos objetos estavam ok.

**As conclusões** observadas na primeira iteração:

- A escala passou nos testes e não há necessidade de alteração no momento
- A ergonomia ficou boa também e não há necessidade de alteração no momento
- O clima não foi marcante para os usuários, a idéia era de ser um ambiente sombrio e misterioso. Há necessidade de alteração na próxima iteração

### Segunda iteração (*Seguda iteração)
**O esboço de interação inicial** foi pensado o espaço fora da masmorra com um painel para iniciar o jogo, o espaço jogo em si e o espaço para o fim do jogo com a possibilidade de reiniciar o mesmo.
![Alt text](docs/esboço_interação_inicial.jpg?raw=true "Esboço de iteração inicial")

**Os paineis de interação** foram adicionados à cena, um para o painel de início e outro para o painel de fim.
![Alt text](docs/paineis_de_interacao.png)

**O raycaster e o sistema de eventos** foram adicionados ao projeto, como teste os botões foram relacionados com métodos do script "GameLogic", mais precisamente o evento "OnClick" dos botões ao método "toggleUI" o qual faz com que os paineis alternem a visibilidade.

tog             |  gle
:-------------------------:|:-------------------------:
![Alt text](docs/tog.png?raw=true "Painel início")  |  ![Alt text](docs/gle.png?raw=true "Painel fim")
O evento "OnClick" funcionou corretamente bem como a chamada ao método. O feedforward do botão também foi implementado alterando a cor do botão quando o usuário está apontando para o mesmo.

**O teste com usuários na primeira iteração**

**Pergunta 1**: O que você achou desse painel? 
**Pertunga 2**: Você entende o que ele quer dizer?
**Pergunta 3**: Você poderia clicar no botão? O que aconteceu?
**Pergunta 4**: O que você achou desse painel? 
**Pertunga 5**: Você entende o que ele quer dizer?
**Pergunta 6**: Você poderia clicar no botão? O que aconteceu?


- Jess disse que o painel estava ok, que conseguia ler tudo e já tinha clicado no botão antes da pergunta e entendeu que era um comportamento de alternancia entre eles. E perguntou quando iria poder jogar de verdade.
- Rena disse que o painel tinha um bom tamanho, comentou que a borda do botão poderia ser um pouco maior porém desse jeito funcionava, e perguntou se era uma funcionalidade em teste, também não precisou ser questionado sobre clicar no botão, o processo foi intuitivo.

**As conclusões** observadas na segunda iteração:

- O entendimento com o painel, leitura e interação foi bem avaliado nesse teste.
- Foi observado nesse segundo teste a ansiedade do usuário de poder jogar o jogo final foi incentivador e deu uma sensação de estar indo pelo caminho certo. E também lembrar da minha própria ansiedade para ver o jogo final, porém entendo a importância das iterações com testes.

### Terceira iteração
**A definição do movimento** foi decidida após o entendimento do fenômeno chamado "Distúrbio de simulador" ([Best Practices | Simulator Sickness | 23](https://scontent.oculuscdn.com/v/t64.5771-25/12482206_237917063479780_486464407014998016_n.pdf?_nc_cat=105&_nc_ht=scontent.oculuscdn.com&oh=11538d0bbc964d4134b4533ed1951540&oe=5C698DEE)), e que ao entrar em caso de distúrbio de simulador dar uma pausa imediatamente ;) . O movimento escolhido foi o de deslizar linearmente e foi feito utilizando a biblioteca [iTween](http://www.pixelplacement.com/itween/documentation.php).

**Os pontos de câmera e movimento** foram definidos em 3, o ponto incial, o ponto do jogo e o ponto final e o movimento da camera foi aplicado a essas três posições. Os eventos são clicar no botão "Start" para a câmera ir para o ponto do jogo, clicar em qualquer lugar para ir para o ponto final e clicar em restart para ser teleportado para o início do jogo.

Ponto incial             |  Ponto do jogo | Ponto final
:-------------------------:|:-------------------------:|:-------------------------:
![Alt text](docs/ponto_inicio.png?raw=true "Painel início")  |  ![Alt text](docs/ponto_jogo.png?raw=true "Painel fim") | ![Alt text](docs/ponto_final.png?raw=true "Painel fim")
O script GameLogic foi alterado e os eventos redefinidos para as novas ações.

Ação usuário | Evento | Ação sistema
:-------------------------:|:-------------------------:|:-------------------------:
buttonClick "Start" | GameLogic.Start() | Movimentar/deslizar câmera para o ponto do jogo
buttonClick "" | GameLogic.PuzzleSuccess() | Movimentar/deslizar câmera para o ponto final
buttonClick "Restart" | GameLogic.Restart() | Movimentar/teletransportar câmera para o ponto inicial

**O teste com usuários na primeira iteração**
Nesta iteração não teve teste com usuário, porém observei os movimentos e achei eles suaves, talvez um pouco lentos mas acho que para a maioria das pessoas irá ficar suave

### Quarta iteração
**A estética das orbs** foi feita a partir da alusão aos 7 chakras do corpo humano trazendo uma imagem para que cada Orb represente um dos chakras e um som padrão.
![Alt text](docs/orbs_images.png?raw=true "Orbs")
Começando da Orb de baixo à esquerda essa simboliza o primeiro chakra e vai na sequencia circular, passando por cima da porta e chegando ao canto inferior direito, o qual simboliza o sétimo e último chakra. 

**Os feedbacks/feedfowards audiovisual** foram implementados, agora o ponto de jogo está jogável com uma certa intuição. 
Para isso após o jogador clicar em "Start" e ir para o ponto de jogo, as Orbs se acendem e tocam um som em uma sequência definida randomicamente e do tamanho definido (neste momento o jogador não pode ter acesso ao "Raycaster" o que gera uma sensação de contemplação já que uma decisão não pode ser tomada). 
![Alt text](docs/display_puzzle.png?raw=true "display puzzle")
Após a sequência apresentada o "Raycaster" é reativado novamente gerando uma ideia que o jogador pode tentar repetir a sequência.
![Alt text](docs/play_puzzle.png?raw=true "play puzzle")

Ao posicionar o "Raycaster" em cima de alguma orb a mesma acende e ao clicar a mesma cresce ,toca um som e diminui, esse som pode ser de acerto ou erro.
![Alt text](docs/orb_hover.png?raw=true "Olhando para a orb")

**Para os sons** foi utilizado o script GvrAudioSource da biblioteca GoogleVR e foi ativado a função "Spatial Audio" (um áudio que é emitido a partir de um local específico no espaço 3D de uma cena) para gera um som espacializado em cada ponto de câmera.

#### Som ambiente nos pontos de câmera
Ponto incial             |  Ponto do jogo | Ponto final
:-------------------------:|:-------------------------:|:-------------------------:
![Alt text](docs/start_soundpoint.png?raw=true "Som início")  |  ![Alt text](docs/play_soundpoint.png?raw=true "Som jogo") | ![Alt text](docs/end_soundpoint.png?raw=true "Som fim")
Sons de grilo | Som sombrio | Sons de grilo

**A mecânica do jogo** funciona da seguinte forma:

1. O jogo apresenta a sequência, tocando um som e acendo alguma das orbs. Obs: O tamanho da sequência pode ser definido no objeto GameLogic.
2. O usuário tem a change de repetir a sequência

	2.1. Caso o usuário acerte a primeira Orb, ela se acende e toca um som de sucesso. E o usuário pode ter a próxima da seqência.
	
	2.2 Caso o usuário erre a Orb da sequência, ela não acende e toca um som de falha e retorna para o passo 1.
3. Caso o usuário acerte todas as Orbs da sequência o jogo acaba, o usuário é movido para o ponto final, é apresentado o painel final e tem a possibilidade de jogar de novo clicando no botão "Restart".

**O teste com usuários na quarta iteração**

Dessa vez o jogo foi entregue aos mesmo testadores sem falar nada a mais sobre o jogo. Após jogarem as seguintes perguntas foram feitas

**Pergunta 1**: O que achou que era para fazer nesse jogo? 

**Pertunga 2**: A sequência das Orbs ficou clara para você?

**Pergunta 3**: Conseguiu perceber que errou?

**Pergunta 3**: Conseguiu perceber os sons ambientes?

**Pergunta 4**: Quais são suas opniões em geral? 

- Jess disse que conseguiu perceber a intenção do jogo, e que depois da terceira vez o jogo ficou chato, pois já tinha visto o cenário todo. Disse que não errou e voltou a jogar e disse que o feedback do erro ficou claro e que o jogo pode ser mais escuro. Em relação aos sons Jess disse que os sons estavam mais no "background" porém gerou um ambiente mais sombrio. 
- Rena disse que a experiência foi boa, que percebeu tudo que tinha para fazer, o erro ficou claro e os sons ambientes traziam uma cena mais escura. Como visão geral disse que seria legal se pudesse ter mais níveis, pois no momento está sendo somente uma aplicação para conhecer uma nova tecnologia o VR.

**As conclusões** observadas na quarta iteração:

- A mecânica do jogo foi bem entendida pelos usuários, bem como seus feedbacks/feedfowards.
- Os sons também foram percebidos de forma positiva
- A iluminação ainda ficou a desejar, o jogo ainda não está sombrio o suficiente, há necessidade de alteração na próxima iteração
- O jogo se mostrou funcional e sem bugs descoberto, porém também se mostrou um entreterimento rápido e sem muitos desafios. Dessa forma na próxima interação a possibilidade de aumentar o tamanho da sequência ao invés de começar de novo.
- Na próxima iteração o jogo irá aumentar a dificuldade e trazer alguns novos elementos visuais.

### Quinta iteração
**A luz** ficou mais escura para trabalhar um ambiente mais sombrio.

![Alt text](docs/luz_mais_escura.png)

**O ponto incial** foi feita uma animação com as Orbs utilizando o animator.

**Para os sons das Orbs** foi aplicado notas de um piano na segunda oitava começando da nota Dó e indo até a nota Si. Os samples foram obtidos no site [University of Iowa Eletronic Music Studios](http://theremin.music.uiowa.edu/MISpiano.html)

**A mecânica de aumentar o nível** de dificuldade foi implementada, no ponto final agora tem um novo botão chamado "One more[Tamanho da próxima sequência]". A cada vez que o usuário clica em "One more[x]" o valor de puzzleLength é aumentado uma unidade > O jogador volta para o ponto de jogo.
A partir do puzzleLength 4 sempre que o usuário finaliza o puzzle com sucesso, a masmorra perde um número específico de elementos aleatóriamente e a luz ambiente rotaciona, assim com o tempo o usuário irá obter mais luz e horizonte. 

Veja o vídeo

[![Alt text](https://img.youtube.com/vi/hmjtj8Bf9so/0.jpg)](https://www.youtube.com/watch?v=hmjtj8Bf9so)
[![Alt text](https://img.youtube.com/vi/hmjtj8Bf9so/0.jpg)](https://www.youtube.com/watch?v=hmjtj8Bf9so)

**O teste com usuários na quarta iteração**

O jogo voltou a ser entregue aos mesmos "testers" e sem explicação sobre o que foi modificado.

**Pergunta 1**: Conseguiu observar as alterações?

**Pertunga 2**: O que achou das mudanças? 

- Jess disse que agora o jogo ficou sombrio de verdade, pelo menos no começo, disse que achou estranho que a masmorra estava ficando meio que retalhada mas curtia ter mais espaço para olhar o espaço, e a luz também permitiu observar mais. Jess também salientou que gostava de observar as mudanças e quase sempre perdia a sequência de apresentação. Em relação ao som, Jess comentou que ficou mais fácil com os sons diferentes, pois ajudava a memorizar com mais clareza
- Rena disso que agora a experiência ficou quase completa, observou que além do dia amanhecer o mesmo entardecia, disse que gostou muito das notas de piano como feedback e que isso ajudou e deixou mais divertido o jogo. Além de observar o desmanche da masmorra, quando não se tinha mais nada acreditou estar em um deserto.

## Teste com usuários e iterações
### Usuários
- Jess: Bacharel em Relações Internacionais
- Rena: Artista visual e entusiasta de entreterimentos imersivos

## Conclusão
Ter trabalhado com teste e iteração foi muito grandioso, parece que o trabalho se dilui, os erros são encontrados mais cedo e se obtém um feedback mais preciso. Trabalhar dessa forma gerou mais conforto e confiança a cada iteração. 
Os desafios para mim foram muitos, como sou novo no unity tive que fazer algumas pesquisas para saber qual era a melhor forma de implementar algumas features e foi bom ter vencido.
Ficou a desejar, da minha parte, uma gestão do projeto com o git.

### Versions Used
- [Unity LTS Release 2017.4.4](https://unity3d.com/unity/qa/lts-releases?version=2017.4)
- Android 9 - OnePlus 6