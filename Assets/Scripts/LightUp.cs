﻿using UnityEngine;
using System.Collections;

public class LightUp : MonoBehaviour {
	public Material lightUpMaterial;
	public GameObject gameLogic;
	private Material defaultMaterial;

	Vector3 bigOrbSize;
	Vector3 origOrbSize;

	// Use this for initialization
	void Start () {
		defaultMaterial = this.GetComponent<MeshRenderer> ().material; //Save our initial material as the default
//		this.GetComponentInChildren<ParticleSystem>().enableEmission = false; //Start without emitting particles
//		em = this.GetComponentInChildren<ParticleSystem>().emission;

		bigOrbSize = this.transform.localScale * 1.2f;
		origOrbSize = this.transform.localScale;

		gameLogic = GameObject.Find ("GameLogic");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void patternLightUp(float duration) { //The lightup behavior when displaying the pattern
		StartCoroutine(lightFor(duration));
	}


	public void gazeLightUp() {
		this.GetComponent<MeshRenderer> ().material.SetFloat ("_Metallic", 0.2f);
		this.GetComponent<MeshRenderer> ().material.SetFloat ("_Glossiness", 0.5f);
//		this.GetComponent<MeshRenderer>().material = lightUpMaterial; //Assign the hover material
//		em.enabled = true; //Turn on particle emmission
//		this.GetComponent<GvrAudioSource>().Play();

		//gameLogic.GetComponent<gameLogic>().playerSelection(this.gameObject);


	}
		
	public void playerSelection() {
		this.transform.localScale = bigOrbSize;
		Invoke ("returnOrbSize", 0.2f);
		gameLogic.GetComponent<GameLogic>().PlayerSelection(this.gameObject);
		this.GetComponent<GvrAudioSource>().Play();
	}
	public void returnOrbSize(){
		this.transform.localScale = origOrbSize;
	}

	public void aestheticReset() {
		this.GetComponent<MeshRenderer> ().material.SetFloat ("_Metallic", 1f);
		this.GetComponent<MeshRenderer> ().material.SetFloat ("_Glossiness", 0f);
		this.GetComponent<MeshRenderer>().material = defaultMaterial; //Revert to the default material
		this.transform.localScale = origOrbSize;
//		em.enabled = false; //Turn off particle emission
	}

	public void patternLightUp() { //Lightup behavior when the pattern shows.
		this.GetComponent<MeshRenderer> ().material.SetFloat ("_Metallic", 0.2f);
		this.GetComponent<MeshRenderer> ().material.SetFloat ("_Glossiness", 0.5f);
//		this.GetComponent<MeshRenderer>().material = lightUpMaterial; //Assign the hover material
//		em.enabled = true; //Turn on particle emmission
		this.GetComponent<GvrAudioSource> ().Play (); //Play the audio attached
	}


	IEnumerator lightFor(float duration) { //Light us up for a duration.  Used during the pattern display
		patternLightUp ();
		yield return new WaitForSeconds(duration-.1f);
		aestheticReset ();
	}
}
