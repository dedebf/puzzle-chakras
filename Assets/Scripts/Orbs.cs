﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orbs : MonoBehaviour {

	public GameObject orb;
	bool isBouncing = false;

	Vector3 bigOrbSize;
	Vector3 origOrbSize;

	// Use this for initialization
	void Start () {
		bigOrbSize = orb.transform.localScale * 1.2f;
		origOrbSize = orb.transform.localScale;
	}
	
	// Update is called once per frame
	void Update () {
		
		
	}

	public void OnClicked(){

	}

	public void OnOver(){
		Debug.Log ("on over");
		orb.transform.localScale = bigOrbSize;
	}

	public void OnOut(){
		Debug.Log ("on out");
		orb.transform.localScale = origOrbSize;
	}
}
